#!/bin/bash

set -e

PARAMSPIDER_EXPECTED_OUTPUT='debian/tests/output/expected_output.txt'
PARAMSPIDER_TMP="${AUTOPKGTEST_TMP:-/tmp}/paramspider_test"

mkdir -p "$PARAMSPIDER_TMP"

# Copy expected output to temporary directory
cp -r "$PARAMSPIDER_EXPECTED_OUTPUT" "$PARAMSPIDER_TMP/"
cd "$PARAMSPIDER_TMP"

if ! paramspider --domain 'testphp.vulnweb.com'; then
  echo "Error: paramspider command failed. Exiting with code 77."
  exit 77
fi

paramspider_output=$(sort < 'results/testphp.vulnweb.com.txt') || exit 77
expected_output=$(sort < 'expected_output.txt')

# Compare the sorted paramspider output with the sorted expected output
#if [ "$paramspider_output" == "$expected_output" ]; then
#    echo "Output matches the content of the file!"
#else
#    echo "Output does not match the expected content."
#    diff -u <(echo "$expected_output") <(echo "$paramspider_output")
#    exit 1
#fi

# Checking if some results are present in paramspider output (fix #1079708)
# Note that future changes in the target website may require adjustments in
# the strings below.
STRING1="^http://testphp.vulnweb.com/index.php?%25id%25=FUZZ&user=FUZZ$"
STRING2="^http://testphp.vulnweb.com/listproducts.php?artist=FUZZ&asdf=FUZZ&cat=FUZZ$"
STRING3="^http://testphp.vulnweb.com/AJAX/infocateg.php?id=FUZZ$"

for i in 1 2 3
do
    grep $STRING$i <(echo "$paramspider_output")
done

# Clean up temporary directory
rm -rf "$PARAMSPIDER_TMP"
